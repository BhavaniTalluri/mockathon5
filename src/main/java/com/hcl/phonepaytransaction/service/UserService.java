package com.hcl.phonepaytransaction.service;

import org.json.JSONException;

import com.hcl.phonepaytransaction.dto.ResponseDto;
import com.hcl.phonepaytransaction.dto.TransferDto;
import com.hcl.phonepaytransaction.dto.UserDto;
import com.hcl.phonepaytransaction.model.Transaction;

public interface UserService {
	public ResponseDto registration(UserDto userDto);
	
	
}
