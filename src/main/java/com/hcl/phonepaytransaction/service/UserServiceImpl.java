package com.hcl.phonepaytransaction.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.hcl.phonepaytransaction.dao.TransactionRepository;
import com.hcl.phonepaytransaction.dao.UserRepository;
import com.hcl.phonepaytransaction.dto.ResponseDto;
import com.hcl.phonepaytransaction.dto.TransferDto;
import com.hcl.phonepaytransaction.dto.UserDto;
import com.hcl.phonepaytransaction.model.Transaction;
import com.hcl.phonepaytransaction.model.User;
@Service
public class UserServiceImpl implements UserService{
	@Autowired
	UserRepository userRepository;

@Autowired
   TransactionRepository transactionRepository;
	@Autowired
	RestTemplate restTemplate;
	User user=new User();
	@Override
	public ResponseDto registration(UserDto userDto) {
		ResponseDto dto=new ResponseDto();
		
		String url = "http://localhost:9094/verify/";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobileNumber",userDto.getMobileNumber());

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		for (Entry<String, Object> entry : params.entrySet()) {
			builder.queryParam(entry.getKey(), entry.getValue());
		}
		boolean result = restTemplate.getForObject(builder.toUriString(),Boolean.class);
		System.out.println(result);
		if(result) {

			BeanUtils.copyProperties(userDto, user);
			userRepository.save(user);
			dto.setMessage("user registered successfully");
		}

		else {
			dto.setMessage("unable to register");
		}
		return dto;
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	
}
