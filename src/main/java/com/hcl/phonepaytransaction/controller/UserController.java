package com.hcl.phonepaytransaction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.phonepaytransaction.dto.ResponseDto;
import com.hcl.phonepaytransaction.dto.UserDto;
import com.hcl.phonepaytransaction.service.UserService;

@RestController
public class UserController {
	@Autowired
	UserService userService;
	
	@PostMapping
	public ResponseDto registration(@RequestBody UserDto userDto) {
		
		return userService.registration(userDto);
		
	}
	

}
