package com.hcl.phonepaytransaction.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.hcl.phonepaytransaction.dto.ResponseDto;
import com.hcl.phonepaytransaction.dto.UserDto;
import com.hcl.phonepaytransaction.model.User;
import com.hcl.phonepaytransaction.service.UserService;


@RunWith(MockitoJUnitRunner.Silent.class)
public class UserControllerTest {
	
	private static final ResponseDto ResponseDto = null;

	@InjectMocks
	UserController userController;

	@Mock
	UserService userService;
	
	
	@Test
	public void registerForPositive() {
		User user=new User("jansi", 956648765, "jansi@gmail.com");
		UserDto dto=new UserDto();
		Mockito.when(userService.registration(dto)).thenReturn(ResponseDto);
		Assert.assertNotNull(user);
		Assert.assertEquals("jansi", user.getUserName());
	}
	
	@Test
	public void registerForNegative() {
		User user=new User(1, "sri", "sri@gmail.com");
		UserDto dto=new UserDto();
		Mockito.when(userService.registration(dto)).thenReturn(ResponseDto);
		Assert.assertNotNull(user);
		Assert.assertEquals("sri@gmail.com", user.getEmail());
	}

}
