package com.hcl.phonepaytransaction.controller;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.hcl.phonepaytransaction.dto.TransferDto;
import com.hcl.phonepaytransaction.model.Transaction;
import com.hcl.phonepaytransaction.service.TransactionService;



@RunWith(MockitoJUnitRunner.Silent.class)
public class TransactionControllerTest {
	
	@InjectMocks
	TransactionController transactionController;

	@Mock
	TransactionService transactionService;
	
	@Test
	public void transferForPositive() throws JSONException {
		Transaction transaction=new Transaction(1, 3000, 96567895, 678568674);
		TransferDto dto=new TransferDto();
		Mockito.when(transactionService.fundTransfer(dto, 1)).thenReturn(transaction);
		ResponseEntity<Transaction> trans = transactionController.transaction(dto, 1);
		Assert.assertNotNull(trans);
		Assert.assertEquals(HttpStatus.OK, trans.getStatusCode());
		
	}
	
	@Test
	public void transferForNegative() throws JSONException {
		Transaction transaction=new Transaction(-1, 3000, 96567895, 678568674);
		TransferDto dto=new TransferDto();
		Mockito.when(transactionService.fundTransfer(dto, 1)).thenReturn(transaction);
		ResponseEntity<Transaction> trans = transactionController.transaction(dto, 1);
		Assert.assertNotNull(trans);
		Assert.assertEquals(HttpStatus.OK, trans.getStatusCode());
		
	}
	

	@Test
	public void transferForException() throws JSONException {
		Transaction transaction=new Transaction(-1, 3000, 96567895, 678568674);
		TransferDto dto=new TransferDto();
		Mockito.when(transactionService.fundTransfer(dto, 1)).thenReturn(transaction);
		ResponseEntity<Transaction> trans = transactionController.transaction(null, 1);
		Assert.assertNotNull(trans);
		Assert.assertEquals(HttpStatus.OK, trans.getStatusCode());
		
	}


}
