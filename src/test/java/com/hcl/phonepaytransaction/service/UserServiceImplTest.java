package com.hcl.phonepaytransaction.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.hcl.phonepaytransaction.dao.UserRepository;
import com.hcl.phonepaytransaction.dto.UserDto;
import com.hcl.phonepaytransaction.model.User;



@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceImplTest {
	
	@InjectMocks
	UserServiceImpl userServiceImpl;

	@Mock
	UserRepository userRepository;
	
	@Test
	public void registerForPositive() {
		User user=new User(1, "jansi",9566497, "jansi@gmail.com");
		@SuppressWarnings("unused")
		UserDto dto=new UserDto();
		Mockito.when(userRepository.save(user)).thenReturn(user);
		Assert.assertNotNull(user);
		Assert.assertEquals("jansi",user.getUserName());
	}
	

	@Test
	public void registerForNegative() {
		User user=new User(-1, "jansi",9566497, "jansi@gmail.com");
		@SuppressWarnings("unused")
		UserDto dto=new UserDto();
		Mockito.when(userRepository.save(user)).thenReturn(user);
		Assert.assertNotNull(user);
		Assert.assertEquals("jansi@gmail.com",user.getEmail());
	}
	

	@Test(expected = NullPointerException.class)
	public void registerForException() {
		User user=new User(-1, "jansi",9566497, "jansi@gmail.com");
		UserDto dto=new UserDto();
		Mockito.when(userRepository.save(user)).thenReturn(user);
		Assert.assertNotNull(user);
		Assert.assertEquals(user,userServiceImpl.registration(dto));
	}


}
