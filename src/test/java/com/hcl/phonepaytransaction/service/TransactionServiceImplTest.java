package com.hcl.phonepaytransaction.service;



import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.phonepaytransaction.dao.TransactionRepository;
import com.hcl.phonepaytransaction.dto.TransferDto;
import com.hcl.phonepaytransaction.model.Transaction;


@RunWith(MockitoJUnitRunner.Silent.class)
public class TransactionServiceImplTest {
	
	
	@InjectMocks
	TransactionServiceImpl transactionServiceImpl;

	@Mock
	TransactionRepository transactionRepository;
	
	

	@SuppressWarnings("deprecation")
	@Test(expected = NullPointerException.class)
	public void transferForNegative() throws JSONException {
		Transaction transaction=new Transaction(-1, 3000, 954673456, 835636987);
		TransferDto  dto=new TransferDto();
		Mockito.when(transactionRepository.save(transaction));
		 transactionServiceImpl.fundTransfer(dto, 1);
		 Assert.assertEquals(3000, transaction.getTransactionamount());
	}
	
	@SuppressWarnings("deprecation")
	@Test(expected = NullPointerException.class)
	public void transferForPositive() throws JSONException {
		Transaction transaction=new Transaction(1, 3000, 954673456, 835636987);
		TransferDto  dto=new TransferDto();
		Mockito.when(transactionRepository.save(transaction));
		 transactionServiceImpl.fundTransfer(dto, 1);
		 Assert.assertEquals(3000, transaction.getTransactionamount());
	}
	

}
